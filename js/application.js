$(document).ready(function(){

  $('.subscribe-name').hide();

  // fix support us black container height
  $(".support-us-container").height($(".support-us-container").height());

  // toggles once and monthly support us options
  $("#support-us-once, #support-us-monthly").on('click', function(){
    $(".paypal-and-credit-card-logo").css({'display':'none'});
    $(".support-us-container").removeClass("support-us-container-hack");
    $("#support-us-once, #support-us-monthly").removeClass("active");
    $(this).addClass("active");
    $('#support-us-once-options, #support-us-monthly-options').hide();
    var elem_id = $(this).attr("id");
    $("#"+elem_id+"-options").slideDown("fast");
  });

  $('.navbar-toggle').on('click', function(){
    if($(window).scrollTop() < 151 && $(window).width() < 768){
      jQuery('#logo').toggleClass('blue-logo');
      $('nav').toggleClass("sticky");
    }

  })

  jQuery(window).scroll(function(){
    var fromTopPx = 150; // distance to trigger
    var scrolledFromtop = jQuery(window).scrollTop();
    if(scrolledFromtop > fromTopPx){
      jQuery('#logo').addClass('blue-logo');

      $('nav').css({
        opacity: scrolledFromtop/ 2
      });
      $('nav').addClass("sticky");

    }else{
      if($(window).width() > 767 || $('.navbar-collapse').hasClass('in') == false){
        jQuery('#logo').removeClass('blue-logo');
        $('nav').css({
          opacity: 1
        });
        $('nav').removeClass("sticky"); 
      }
      
    }
  });

  // Validate the contact form    
	$('#commentForm').validate({
    rules: {
      name: {
        minlength: 2,
        required: true
      },
      org_representator: {
        required: true
      },
      country:{
        required: true
      },
      email: {
          required: true,
          email: true
      },
      phone: {
          minlength: 8,
          required: true
      },
      'query_about[]':{
        required: true,
        minlength: 1
      }
	  },
    highlight: function (element) {	    		
      $(element).closest('.form-group').addClass('has-error');
    },
    success: function (element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      element.remove();;
    },
    submitHandler: function(form) {
    
      var formData = {'name': $(form).find("input#name").val(), 'org_representator': $(form).find('input[name="org_representator"]:checked').val(), 
                      'email': $(form).find("input#email").val(), 'organisation_name': $(form).find('input[name="organisation_name"]').val(),
                      'country': $(form).find('select[name="country"]').val(), 'city': $(form).find('input[name="city"]').val(), 
                      'phone': $(form).find("input#phone").val(), 'message': $(form).find("input#message").val(), 
                      'query_about': $(form).find('input[name="query_about"]:checked').val() };

      $(form).ajaxSubmit({
        type: "POST",
        url: $(form).attr('action'),
        data: formData,
        beforeSubmit: function(){
          var $submitBtn = $("#submit-btn");
          $submitBtn.attr("value", "Submitting...");
          $submitBtn.attr('disabled','disabled');
        },
        success: function(){
            var $submitBtn = $("#submit-btn");
            $submitBtn.attr("value","Submit");
            $submitBtn.removeAttr('disabled');
            window.location.href = "/";
        }
      });
    }
	});

  // Shows all the sunscribe fields
  $("input[name='EMAIL']").focus(function(){
    if($('.subscribe-name').is(':visible') == false){
      $('.subscribe-name').slideDown();
      $('input[name="FNAME"]').focus();
      $(this).removeClass('mce_inline_error');
    }
  });

  var iframe = $('#welcomeVideoObject')[0];
  var player = $f(iframe);

  $(".play-video").click(function(){
    $("#welcomeVideoObject").css("display","block");
    player.api("play");
  });

  player.addEvent('ready', function() {
    player.addEvent('pause', onPause);
  });

  function onPause(id) {
    $("#welcomeVideoObject").css("display","none");
  }

});

function replaceTwitterContent(){
  var $widgetContent =$('#twitter-widget-0').contents();
  var $headers = $widgetContent.find('.header');
  var $tweets = $widgetContent.find('.h-entry');

  $.each($headers, function(i,v){
    var tweet = $tweets[i];
    var tweetTime = $(v).find(".dt-updated").html();
    $(tweet).find(".e-entry-content").after("<div class='tweet-time'>"+tweetTime+"</div>");
  });

  $headers.html("<img src='images/icon-twitter.png' class='avatar'/>");
  $widgetContent.find(".var-narrow .tweet .header").css({'min-height' : '0px'});
  $widgetContent.find(".var-chromeless .tweet").css({'padding': '5px 2px 5px 30px', 'color' : '#95A5A6', 'font-family' : 'lato'});
  $widgetContent.find(".timeline .e-entry-title").css({'font-size' : '14px', 'line-height' : '16px'});
  $widgetContent.find(".avatar").css({"left": "-30px", "width" : "24px", "height" : "24px", "background" : "transparent"});
  $widgetContent.find(".tweet-time").css({'font-weight': 'bold', 'text-align': 'left', 'font-size' : '14px', 'font-family' : 'lato'});
}