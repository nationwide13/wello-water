<?php
  if (!empty($_POST['email']) && !empty($_POST['name']) && !empty($_POST['phone']) && !empty($_POST['query_about']) && !empty($_POST['country']) && !empty($_POST['org_representator']) ) {
    $to = "sales@wellowater.org"; // this is your Email address
    
    $from = $_POST['email']; // this is the sender's Email address
    $name = $_POST['name'];
    $subject = "Website sales inquiry form";
    $message = "Name : ". $name . "\n\n" . "Who do you represent : " . $_POST['org_representator'] ."\n\n" . "Organisation Name : " . $_POST['organisation_name'] . "\n\n" . "Location : " . $_POST['country'] . "\n\n" . "City : " . $_POST["city"] . "\n\n" . "Email : " . $from . "\n\n" . "Phone Number : " . $_POST['phone'] . "\n\n" . "Interested In : ". $_POST['query_about'] . "\n\n" . "Message : " . $_POST['message'];

    $headers = "From:" . $from;

    mail($to,$subject,$message,$headers);

    echo "Mail Sent. Thank you " . $name . ", we will contact you shortly.";
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    // You cannot use header and echo together. It's one or the other.
  }
?>
