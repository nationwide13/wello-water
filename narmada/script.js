$(window).load(function() {
$('#loading').hide();
});

$(document).ready(function () {
    
	if (!$.support.transition)
  $.fn.transition = $.fn.animate;
    
    $("#button1").click(function () {
        $("#image1").transition({ x: '-2000px' }, 1000 , function(){
		("#image1").remove()
		});
        $("#button1").transition({ x: '-2000px' }, 1000);
		});
		
	$("#button2").click(function () {
        $("#image2").transition({ x: '-2000px'}, 1000, function(){
		("#image2").remove()
		}		
		);
        $("#button2").transition({  x: '-2000px' }, 1000);
		});
		
	$("#button3").click(function () {
        $("#image3").transition({ x: '-2000px' }, 1000, function(){
		("#image3").remove()
		} );
        $("#button3").transition({ x: '-2000px' }, 1000);
		});	
		
		
	$("#button4").click(function () {
        $("#image4").transition({ x: '-2000px' }, 1000, function(){
		("#image4").remove()
		} );
        $("#button4").transition({ x: '-2000px' }, 1000);
		$("#image4-info").remove();
        $("#image4-i").remove();
		});	

	$("#image4-i").mouseenter(function () {
        $("#image4-info").transition({ y: '500px' }, 1000 );
        
		});		
		
	$("#button5").click(function () {
        $("#image5").fadeOut(200);
        $("#button5").fadeOut(200);
		});	
		
	$("#button6").click(function () {
        $("#image6").transition({  x: '-2000px' }, 1000 , function(){
		("#image6").remove()
		});
		$("#button6").transition({ x: '-2000px'}, 1000);
        });

	$("#button7").click(function () {
        $("#image7").transition({ x: '-2000px' }, 1000, function(){
		("#image7").remove()
		} );
        $("#button7").transition({ x: '-2000px' }, 1000);
		});
		
	$("#button8").click(function () {
        $("#image8").transition({  x: '-2000px' }, 1000 , function(){
		("#image8").remove()
		});
		$("#button8").transition({  x: '-2000px'}, 1000);
        });
		
		$("#button10").click(function () {
        $("#image10").fadeOut(200);
        $("#button10").fadeOut(200);
		});	
		
	$("#button11").click(function () {
        $("#image11").transition({ x: '-2000px' }, 1000 , function(){
		("#image11").remove()
		});
		$("#button11").transition({ x: '-2000px' }, 1000);

		$("#page12TopText").delay( 500 ).fadeIn("slow");
		$("#image12").delay( 1500 ).fadeIn("slow").delay( 1500 ).fadeOut("slow");
		$("#image12f").delay( 1500 ).fadeIn("slow");
		
		$("#image13").delay( 3000 ).fadeIn("slow").delay( 1000 ).fadeOut("slow");
		$("#image13f").delay( 3000 ).fadeIn("slow");
		
		$("#image14").delay( 4500 ).fadeIn("slow")
		$("#image14t").delay( 5500 ).fadeIn("slow");

		$("#buttonD").delay( 6500 ).fadeIn("slow")
		$("#buttonD1").delay( 6500 ).fadeIn("slow");
		
        });

	$("#buttonD1").click(function () {
		$("#buttonD1")
		$("#twitter").show().transition({ y: '60' }, 1000);
		$("#facebook").show().transition({ y: '60' }, 1000);
		$("#linkedin").show().transition({ y: '60' }, 1000);

	});
		
});